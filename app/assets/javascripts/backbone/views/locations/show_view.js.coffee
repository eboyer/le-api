LeApi.Views.Locations ||= {}

class LeApi.Views.Locations.ShowView extends Backbone.View
  template: JST["backbone/templates/locations/show"]

  render: ->
    @$el.html(@template(@model.toJSON() ))
    return this
