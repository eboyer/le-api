class LeApi.Models.Location extends Backbone.Model
  paramRoot: 'location'

  defaults:
    title: null

class LeApi.Collections.LocationsCollection extends Backbone.Collection
  model: LeApi.Models.Location
  urlRoot: 'http://stormy-thicket-4906.herokuapp.com/locations'
  url: '/locations'
